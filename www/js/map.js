let eigeneLatitude = 51.505;
let eigeneLongitude = -0.09;
let markers;
let mymap = L.map('mapid');

ansichtEinstellen(eigeneLatitude, eigeneLongitude);

$.ajax({
    url: 'https://orientierung.herokuapp.com/group/',
    success: function( response ) {
        for(var i in response.groups) {
            $('#availableGroups').append(`<option value="${response.groups[i].id}">${response.groups[i].name}</option>`)
        };
    }
});

$('#availableGroups'). change(function(){
    var placeId = $(this). children("option:selected"). val();
    $.ajax({
        url: 'https://orientierung.herokuapp.com/group/' + placeId + '/place',
        success: function( response ) {
            for(var i in response.places) {
                var placeLatitude = response.places[i].lat;
                var placeLongitude = response.places[i].lng;
                ansichtEinstellen(placeLatitude, placeLongitude);
            }
        }
    });
});


function ansichtEinstellen(eigeneLatitude, eigeneLongitude) {
    mymap.setView([eigeneLatitude, eigeneLongitude], 13);
    markers = L.marker([eigeneLatitude, eigeneLongitude]).addTo(mymap);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFyaW5iYWxhYmFub3YiLCJhIjoiY2tjYThhMzFqMXN2czJzbzBmYno3MXptdyJ9.rK2oyLHJFJ2ZjivT4SwMBw'
    }).addTo(mymap);
};


/*
var circle = L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);
*/


$("#standortbutton").on("click", function(){
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
});
$("#clearMarkers").on("click", function(){
    markers.clearLayers();
});

var onSuccess = function(position) {
    eigeneLatitude = position.coords.latitude;
    eigeneLongitude = position.coords.longitude;
    ansichtEinstellen(eigeneLatitude, eigeneLongitude);
};

function onError(error) {
    alert('Geolocation Error: Please allow the app to access your geolocation');
}

/*
function koordinatenHolen {
    $.ajax({
        url: '/XXX',
        success: function( response ) {
            var geholteKoordinaten = jQuery.parseJSON(response);

            }
        }
}
*/


